<%@ page import="avetmiss.Batch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Batch List</title>
	</head>
	<body>
		<div class="content scaffold-list" role="main">
			<h1>Batch List</h1>
			<div id="findBatch">
			<g:field type="text" id="findbatchid" name="batchIdTextFind" value=""/>
			<g:actionSubmit type="button" name="batchIdButtonFind" value="Find Batch" action="returnBatch(document.getElementById('findbatchid').value"/>
			</div>
			<g:if test="${flash.message}"><div class="message" role="status">${flash.message}</div></g:if>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="batchid" title="Batch Id" />
						<g:sortableColumn property="crateDate" title="Create Date" />
						
						<g:sortableColumn property="batchProcess" title="Batch Process" colspan="2"/>
					</tr>
				</thead>
				<tbody>
				<g:each in="${batchInstanceList}" status="i" var="batchInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="download" id="${batchInstance.id}">${batchInstance.batchId}</g:link></td>
						<td><g:formatDate date="${batchInstance.createDate}" /></td>
						<td><g:actionSubmit value="Create NAT" controller="Batch" action="createNAT" id="${batchInstance.id}"/></td>
						<td><g:actionSubmit value="Delete Batch" controller="batch" action="delete" id="${batchInstance.id}"/></td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
			</div>
		</div>
	</body>
</html>
