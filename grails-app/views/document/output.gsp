<%@ page import="uploadFile.Document" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta name="layout" content="main"/>
<title>Batch OutPut</title>
</head>
<body>
  <div class="body">
		<div class="content scaffold-list" role="main">
			<h1>Batch Process Results</h1>
			<div id="batchId"><p><strong>Batch Id: ${batchId}</strong></p></div>
			<g:if test="${flash.message}"><div class="message" role="status">${flash.message}</div></g:if>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="Error" title="Error" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${errorsList}" status="i" var="errors">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${errors}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div> 
		<div class="pagination"><p><strong>Errors Found in File is: ${numberOfErrorsInBatch}</strong></p></div> 
  </div>
</body>
</html>