<%@ page import="uploadFile.Document" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Files List</title>
	</head>
	<body>

		<div class="nav" role="navigation">
			<ul><li><g:link class="create" action="create">Upload New File</g:link></li></ul>
		</div>
		<div class="content scaffold-list" role="main">
			<h1>Files List</h1>
			<g:if test="${flash.message}"><div class="message" role="status">${flash.message}</div></g:if>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="filename" title="File Name" />
						<g:sortableColumn property="uploadDate" title="Upload Date" />
						<g:sortableColumn property="fileProcess" title="File Process" colspan="2"/>
					</tr>
				</thead>
				<tbody>
				<g:each in="${documentInstanceList}" status="i" var="documentInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="download" id="${documentInstance.id}">${documentInstance.filename}</g:link></td>
						<td><g:formatDate date="${documentInstance.uploadDate}" /></td>
						<td><g:link action="createBatch" id="${documentInstance.id}"><input type="button" value="Create Batch"></g:link></td>
						<td><g:link action="delete" id="${documentInstance.id}"><input type="button" value="Delete"></g:link></td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${documentInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
