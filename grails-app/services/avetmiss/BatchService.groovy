package avetmiss

import grails.transaction.Transactional
import java.util.Random;
import uploadFile.Document
import avetmiss.Batch

@Transactional
class BatchService {
	
	Batch makeBatch(def processedFile){
		Batch newBatch = new Batch()
		Random r = new Random()
		newBatch.batchId = r.nextInt()
		Integer currentLine = 0
		
		/// entering every line data into a record
		processedFile.splitEachLine(',') {fields ->
				def newRecord = new Record(
					membershipNumber: fields[0]?.trim(),
					firstName: fields[1]?.trim(),
					lastName: fields[2]?.trim(),
					courseCost: fields[3]?.trim(),
					batch:newBatch
				)

		//Checking New Record Has no errors
		newRecord.validate()	
		// if it has errors record that and record error line
		if (newRecord.hasErrors()){
			newRecord.errors.allErrors.each {
				newBatch.numberOfErrors++
				newBatch.validationErrors.add(it.objectName + ':' + it.field + " - in Line " + currentLine.toString())
				}
			}
		//if does not have errors add the record to the batch
		else {
			newBatch.addToRecords(newRecord)
			}
		currentLine++
		}
		
		newBatch.save()
	}
	
	def displayBatch(Batch getBatch){
		
		return newBatch
	}
}
