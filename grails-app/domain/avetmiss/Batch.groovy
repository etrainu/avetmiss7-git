package avetmiss

import java.util.Date;

class Batch {
	Integer batchId
	Date createDate = new Date()
	Integer	numberOfErrors = 0
	List<String> validationErrors = new ArrayList<String>()
	
	static hasMany = [records: Record]
	
	static transients = [ "validationErrors"]
    
	static constraints = {
    }
}
