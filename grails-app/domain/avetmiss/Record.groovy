package avetmiss

class Record {
	Integer memberNumber
	String	firstName
	String	lastName
	float	courseCost
	
	static belongsTo = [batch: Batch]
    
	static constraints = {
		memberNumber ()
		firstName()
		lastName ()
		courseCost()
		
    }
}
