package avetmiss


class BatchController {
	def batchService
	def index() {
		redirect(action: "listBatches", params: params)
	}
	
	def listBatches() {
		params.max = 10
		[batchInstanceList: Batch.list(params), batchInstanceTotal: Batch.count()]
	}
	
	def returnBatch(long id){
		Batch getBatch = Batch.get(id)
		batchService.displayBatch(getBatch)
		
		if ( getBatch == null) {
			flash.message = "Batch not found."
			redirect (action:'listBatches')
		} else {
		getBatch.list()
		}
	}
	
	def downloadBatch(long id){
		Batch batchInstance = Batch.get(id)
		if ( batchInstance == null) {
			flash.message = "Batch not found."
			redirect (action:'listBatches')
		} else {
			response.setContentType("APPLICATION/OCTET-STREAM")
			response.setHeader("Content-Disposition", "Attachment;Filename=\"${documentInstance.filename}\"")

			def file = new File(batchInstance.fullPath)
			def fileInputStream = new FileInputStream(file)
			def outputStream = response.getOutputStream()

			byte[] buffer = new byte[4096];
			int len;
			while ((len = fileInputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, len);
			}

			outputStream.flush()
			outputStream.close()
			fileInputStream.close()
		}
	}
	
	def deleteBatch(long id){
		Batch recordInstance = Batch.get(id)
		if (recordInstance == null) {
			notFound()
			return
		}

		recordInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [message(code: 'Record.label', default: 'Record'), recordInstance.id])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}
}
